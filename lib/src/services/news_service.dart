import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:newsapp/src/models/category_model.dart';
import 'package:newsapp/src/models/news_models.dart';
import 'package:http/http.dart' as http;

final _url_news = 'https://newsapi.org/v2';
final _apikey = 'be55d72a7e06459d8deb6375450dc4e5';

class NewsService with ChangeNotifier {
  List<Article> headlines = [];
  String _selectedCategory = 'business';
  List<Category> categorys = [
    Category(FontAwesomeIcons.building, 'business'),
    Category(FontAwesomeIcons.tv, 'entertainment'),
    Category(FontAwesomeIcons.addressCard, 'general'),
    Category(FontAwesomeIcons.headSideVirus, 'health'),
    Category(FontAwesomeIcons.vials, 'science'),
    Category(FontAwesomeIcons.volleyballBall, 'sports'),
    Category(FontAwesomeIcons.memory, 'technology'),
  ];

  Map<String,List<Article>> categoryArticle = { 

  };

  NewsService() {
    this.getTopHeadlines();
    categorys.forEach((item) {
      this.categoryArticle[item.name] =  [];
     });
  }

  get selectedCategory => this._selectedCategory;
  set selectedCategory (String valor){
    this._selectedCategory = valor;
    this.getArticlesByCategory(valor);
    notifyListeners();
  }

  getTopHeadlines() async {
    final url = '$_url_news/top-headlines?apiKey=$_apikey&country=us';
    final resp = await http.get(Uri.parse(url));
    final newsResponse = newsResponseFromJson(resp.body);

    this.headlines.addAll(newsResponse.articles);
    notifyListeners();
  }

  getArticlesByCategory(String category) async{ 

    if(this.categoryArticle[category].length > 0){
      return this.categoryArticle[category];
    }

    final url = '$_url_news/top-headlines?apiKey=$_apikey&country=us&category=$category';
    final resp = await http.get(Uri.parse(url));
    final newsResponse = newsResponseFromJson(resp.body);
    this.categoryArticle[category].addAll(newsResponse.articles);
    notifyListeners();
  }
  List<Article>get getArticulosCategoriasSeleccionada => this.categoryArticle[this.selectedCategory];
}
